# Working on files #

# README #

Actual state on [this wiki](https://bitbucket.org/a8mmi4i1y/projectx/wiki/Actual%20state).

Description of first-run, settings and working with Eclipse & BitBucket.

### Documentation ###
Actual state of documentation is on Google Disk:
[ProjectX](https://drive.google.com/drive/folders/0B3mQ3wr3lZEGfldmSEVYNnhRazkzMWJYc1Z4Z0otekJGb0xaVFBPZERqWHpPNG4yR2IxV0U)

### Development tools ###
JetBrains **PyCharm** - [jetbrains.com](https://www.jetbrains.com/student/)

(JetBrains IntelliJ for Java)

**FREE** for students for 1 year.

### Prerequisites ###

* [Eclipse Luna](https://eclipse.org/downloads/),
* [Python 3.2.5 (32 bit)](https://www.python.org/download/releases/3.2.5/),
* [Pygame 1.9.2 win32 for python 3.2](http://www.pygame.org/download.shtml),
* *(recomanded)* eGit pro Eclipse ([tutorial](http://crunchify.com/how-to-configure-bitbucket-git-repository-in-you-eclipse/)).

### First commit ###
([tutorial](http://crunchify.com/how-to-configure-bitbucket-git-repository-in-you-eclipse/) - Step-7 and above)

* Clone repository,
* make changes,
* commit and push.

### Team ###

* Marek Bařina
* Patrik Markovič
* Tomáš Kadavý
* Roman Janás