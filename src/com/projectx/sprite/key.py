import pygame

from com.projectx.util.animation import Animation
from com.projectx.util.configuration import Configuration


class Key(pygame.sprite.Sprite):
    """ Platform the user can jump on """

    def __init__(self, position, idx):
        """ Platform constructor. Assumes constructed with user passing in
            an array of 5 numbers like what's defined at the top of this
            code. """
        super().__init__()
        self.id = idx
        self.config = Configuration()
        self.key_sound = pygame.mixer.Sound(self.config.get_string("key_sound_path", "Sound"))
        self.key_img = Animation(self.config.get_string("key_image_path", "Graphics"))
        self.image = self.key_img.get_image()
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = position
        self.id = idx  # must be unique

    def update(self, hit_list, key_pressed, figure):
        if hit_list is not None and key_pressed:
            for collided_key in hit_list:
                if collided_key.id == self.id:
                    # TODO inventory
                    figure.inventory.append("key-" + str(self.id))
                    self.kill()
                    hit_list.remove(collided_key)
                    collided_key.kill()
                    self.key_sound.play(0)