import pygame
from com.projectx.util.configuration import Configuration


class ChestSprite(pygame.sprite.Sprite):
    closed = pygame.image.load('img/chest-close.png')
    opened = pygame.image.load('img/chest-open.png')

    def __init__(self, position, idx):
        pygame.sprite.Sprite.__init__(self)
        self.config = Configuration()
        self.rect = self.opened.get_rect()
        self.image = self.closed
        self.rect.center = position
        self.id = idx  # must be unique
        self.remove_countdown_started = False
        self.remove = False
        self.REMOVE_COUNTDOWN = 60
        self.chest_to_remove = None
        self.chest_open_sound = pygame.mixer.Sound(self.config.get_string("chest_sound_path", "Sound"))

    def update(self, hit_list, key_pressed, figure):
        if self.remove_countdown_started:
            self.remove_countdown()
            if self.remove and self.id is self.chest_to_remove.id:
                self.kill()
                if self.chest_to_remove is not None:
                    figure.inventory.append("chest-" + str(self.chest_to_remove.id))
                    self.chest_to_remove.kill()
                    if self.chest_to_remove in hit_list:
                        hit_list.remove(self.chest_to_remove)
        if hit_list is not None and key_pressed:
            for collided_chest in hit_list:
                if collided_chest.id == self.id:
                    if "key-" + str(collided_chest.id) in figure.inventory:
                        figure.inventory.remove("key-" + str(collided_chest.id))
                        self.image = self.opened
                        self.chest_open_sound.play(0)
                        self.remove_countdown_started = True
                        self.chest_to_remove = collided_chest
                else:
                    self.image = self.closed

    def remove_countdown(self):
        if self.REMOVE_COUNTDOWN > -1:
            self.REMOVE_COUNTDOWN -= 1
            if self.REMOVE_COUNTDOWN == 0:
                self.remove = True