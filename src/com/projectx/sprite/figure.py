import pygame

from com.projectx.util.animation import Animation
from com.projectx.util.configuration import Configuration
from com.projectx.util.enum import Enum
from com.projectx.util import level


class FigureSprite(pygame.sprite.Sprite):
    def __init__(self, position):

        """Initialise figure. Heading right, standing at self.position."""
        self.config = Configuration()
        self.STEP = self.config.get_float("movement_speed", "Figure")
        self.GRAVITY = self.config.get_float("gravity", "Figure")
        pygame.sprite.Sprite.__init__(self)
        self.animation = Animation("img/figure")
        self.x, self.y = self.position = position

        self.chest_collisions = None
        self.key_collisions = None

        # door
        self.door_collisions = None

        self.show_action_key = False

        self.jump_sound = pygame.mixer.Sound(self.config.get_string("jump_sound_path", "Sound"))
        self.fall_sound = pygame.mixer.Sound(self.config.get_string("fall_sound_path", "Sound"))

        self.FLOOR = 440
        self.change_x = 0
        self.change_y = 0

        # starting number of lives
        self.lives = 3

        # inventory
        self.inventory = []
        # self.inventory.append("key1")
        # self.inventory.append("key2")
        self.INVENTORY_CAPACITY = 3

        # falling
        self.fall_counter = 0
        self.MAX_FALL = 190

        self.image = self.animation.get_image()
        self.rect = self.image.get_rect()
        self.rect.center = self.position

        # self.level = level.Level_01(self.rect)
        self.level = level.Level_tut_01(self.rect)

        self.is_on_door = False

    def update(self):
        """ Move the player. """

        # update cycles counter
        if self.change_x is not 0:
            self.animation.update_counter()
            self.image = self.animation.get_image()

        # Gravity
        self.calculate_gravity()

        # Move left/right
        self.rect.x += self.change_x

        # See if we hit anything
        block_hit_list = pygame.sprite.spritecollide(self, self.level.platform_list, False)
        for block in block_hit_list:
            # If we are moving right,
            # set our right side to the left side of the item we hit
            if self.change_x > 0:
                self.rect.right = block.rect.left
            elif self.change_x < 0:
                # Otherwise if we are moving left, do the opposite.
                self.rect.left = block.rect.right

        self.show_action_key = False

        # See if we hit anything
        self.key_collisions = pygame.sprite.spritecollide(self, self.level.key_list, False)
        if len(self.key_collisions) > 0:
            self.show_action_key = True

        # See if we hit anything
        self.chest_collisions = pygame.sprite.spritecollide(self, self.level.chest_list, False)
        if len(self.chest_collisions) > 0:
            self.show_action_key = True

        # See if we hit anything - door
        self.door_collisions = pygame.sprite.spritecollide(self, self.level.door_list, False)
        self.is_on_door = False
        if len(self.door_collisions) > 0:
            self.show_action_key = True
            self.is_on_door = True

        # Move up/down
        self.rect.y += self.change_y

        # fall counter, if it reaches a critical measure it hurts you
        if abs(self.change_y) > 1:
            self.fall_counter += abs(self.change_y)
        else:
            if self.fall_counter >= self.MAX_FALL:
                self.fall_sound.play(0)
                self.lives -= 1
            self.fall_counter = 0

        # Check and see if we hit anything
        block_hit_list = pygame.sprite.spritecollide(self, self.level.platform_list, False)
        for block in block_hit_list:
            # Reset our position based on the top/bottom of the object.
            if self.change_y > 0:
                self.rect.bottom = block.rect.top
            elif self.change_y < 0:
                self.rect.top = block.rect.bottom

            # Stop our vertical movement
            self.change_y = 0

    def calculate_gravity(self):
        """ Calculate effect of gravity. """
        # x, y = self.position
        if self.change_y == 0:
            self.change_y = 1
        else:
            self.change_y += self.GRAVITY

        # See if we are on the ground.
        if self.rect.y >= self.FLOOR - self.rect.height and self.change_y >= 0:
            self.change_y = 0
            self.rect.y = self.FLOOR - self.rect.height

    def jump(self):
        """ Called when user hits 'jump' button. """
        # move down a bit and see if there is a platform below us.
        # Move down 2 pixels because it doesn't work well if we only move down 1
        # when working with a platform moving down.
        self.rect.y += 2
        platform_hit_list = pygame.sprite.spritecollide(self, self.level.platform_list, False)
        key_hit_list = pygame.sprite.spritecollide(self, self.level.key_list, False)
        chest_hit_list = pygame.sprite.spritecollide(self, self.level.chest_list, False)
        self.rect.y -= 2

        # If it is ok to jump, set our speed upwards
        if len(platform_hit_list) > 0 or len(key_hit_list) > 0 or len(
                chest_hit_list) > 0 or self.rect.bottom >= self.FLOOR:
            self.change_y = -8
            self.jump_sound.play(0)

    def move_left(self):
        """ Called when the user hits the left arrow. """
        self.animation.set_direction(Enum.direction().left)
        self.change_x = -self.STEP

    def move_right(self):
        """ Called when the user hits the right arrow. """
        self.animation.set_direction(Enum.direction().right)
        self.change_x = self.STEP

    def stop_moving(self):
        """ Called when the user lets off the keyboard. """
        self.change_x = 0
