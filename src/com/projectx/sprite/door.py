import pygame
from com.projectx.util.configuration import Configuration


class Door(pygame.sprite.Sprite):
    """ Door for end level """

    def __init__(self, position):
        super().__init__()
        self.config = Configuration()
        # TODO sound to door
        self.door_Sound = None
        self.door_img = pygame.image.load('img/door.png')
        self.image = self.door_img
        self.rect = self.image.get_rect()
        self.rect.center = position

    def update(self, hit_list, key_pressed, player):
        pass