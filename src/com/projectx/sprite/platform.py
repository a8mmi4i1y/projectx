import pygame
from com.projectx.util.configuration import Configuration


class Platform(pygame.sprite.Sprite):
    """ Platform the user can jump on """
 
    def __init__(self, position):
        """ Platform constructor."""
        pygame.sprite.Sprite.__init__(self)
        self.config = Configuration()
        self.image = pygame.image.load(self.config.get_string("platform_image_path", "Graphics"))
        self.rect = self.image.get_rect()
        # self.rect.center = position
        self.rect.x, self.rect.y = position
