# INITIALISATION
from sys import exit

import pygame
from pygame.locals import KEYDOWN, KEYUP, K_UP, K_LEFT, K_RIGHT, K_ESCAPE, K_x, K_o, K_p, K_s

from com.projectx.sprite.figure import FigureSprite
from com.projectx.util import level
from com.projectx.util.gui import Gui
from com.projectx.util.configuration import Configuration


class Game():
    screen = None
    clock = None
    figure_group = None
    chest_group = None
    figure = None
    background = None
    platform = None
    key = None
    gui = None
    # time for level
    time = None
    TIME_TICK = 0.012
    # menu = GameMenu()

    def __init__(self, is_continue=False):
        self.is_paused = False
        self.config = Configuration()
        self.isRunning = True
        self.run = True
        pygame.init()
        self.v_res = self.config.get_integer("v_res", "Graphics")
        self.h_res = self.config.get_integer("h_res", "Graphics")
        self.screen = pygame.display.set_mode((self.v_res, self.h_res))

        self.active_sprite_list = pygame.sprite.Group()

        # CREATE A FIGURE AND RUN
        self.figure = FigureSprite((160, 440))
        self.figure_group = pygame.sprite.RenderPlain(self.figure)

        self.active_sprite_list.add(self.figure_group)

        # Tutorial levels
        self.l_tut1 = level.Level_tut_01(self.figure)

        self.l1 = level.Level_01(self.figure)
        self.l2 = level.Level_02(self.figure)
        self.l3 = level.Level_03(self.figure)

        self.level_list = []
        self.level_list.append(self.l_tut1)
        self.level_list.append(self.l1)
        self.level_list.append(self.l2)
        self.level_list.append(self.l3)

        # Set the current level
        if is_continue:
            self.current_level_no = self.config.get_integer("game_level", "General")
            self.figure.inventory = []
            self.figure.lives = 3
            self.time = 30
        else:
            self.current_level_no = 0
        if self.current_level_no < 4:
            self.current_level = self.level_list[self.current_level_no]
        else:
            self.current_level = level.Random_level(self.figure)
        if is_continue:
            self.figure.level = self.current_level
            self.figure.rect.x = 160
            self.figure.rect.y = 440

        # start time for level
        if self.current_level_no == 0:
            self.time = 100
        else:
            self.time = 30

        # GUI
        self.gui = Gui(self.screen, self.v_res, self.h_res)

    def tut_messages(self):
        if self.current_level_no == 0:
            self.gui.show_skip()
            if self.time <= 100 and self.time >= 97:
                self.gui.show_tut_message(1)
            if self.time <= 96 and self.time >= 92:
                self.gui.show_tut_message(2)
            if self.time <= 92 and self.time >= 89:
                self.gui.show_tut_message(3)
                level.Level_tut_01.enable_door(self.current_level)
            if self.time <= 88 and self.time >= 86:
                self.gui.show_tut_message(4)
            if self.time <= 85 and self.time >= 81:
                self.gui.show_tut_message(5)
                level.Level_tut_01.enable_chest(self.current_level)
            if self.time <= 80 and self.time >= 78:
                self.gui.show_tut_message(6)
            if self.time <= 77 and self.time >= 74:
                self.gui.show_tut_message(7)
                level.Level_tut_01.enable_key(self.current_level)
            if self.time <= 73 and self.time >= 70:
                self.gui.show_tut_message(8)
            if self.time <= 69 and self.time >= 66:
                self.gui.show_tut_message(9)
            if self.time <= 65 and self.time >= 64:
                self.figure.inventory.append("chest-2")
                self.time = 63
            if self.time <= 62 and self.time >= 0:
                self.gui.show_tut_message(10)

            self.figure.level = self.current_level

        self.win_sound = pygame.mixer.Sound(self.config.get_string("win_sound_path", "Sound"))

    def next_level(self):
        if "chest-1" in self.figure.inventory and "chest-2" in self.figure.inventory:
            self.current_level_no += 1
            self.config.set_attr("General", "game_level", str(self.current_level_no))
            if self.current_level_no > len(self.level_list) - 1:
                # end game
                # self.config.set_attr("General", "game_level", str(self.current_level_no - 1))
                # pygame.mixer.music.stop()
                self.win_sound.play(0)
                # self.stop()
                self.figure.inventory = []
                self.figure.lives = 3
                self.time = 30
                self.current_level = level.Random_level(self.figure)
                self.figure.level = self.current_level
            else:
                self.current_level = self.level_list[self.current_level_no]
                self.figure.level = self.current_level
                self.figure.inventory = []
                self.figure.lives = 3
                self.time = 30
                self.figure.rect.x = 160
                self.figure.rect.y = 440

    def start(self):
        # LOAD SOUNDS
        bg_sound = pygame.mixer.music.load(self.config.get_string("bg_sound_path", "Sound"))
        walk_sound = pygame.mixer.Sound(self.config.get_string("walk_sound_path", "Sound"))
        walk_sound.set_volume(0.4)
        fall_sound = pygame.mixer.Sound(self.config.get_string("fall_sound_path", "Sound"))
        game_over_sound = pygame.mixer.Sound(self.config.get_string("game_over_sound_path", "Sound"))

        if self.config.get_bool("bg_sound_enabled", "Sound"):
            pygame.mixer.music.play(-1)
            
        # self.isRunning = True
        while self.run:
            if self.isRunning:
                # USER INPUT
                for event in pygame.event.get():
                    if not hasattr(event, 'key'):
                        continue
                    if event.type == KEYDOWN:
                        if event.key == K_RIGHT:
                            walk_sound.play(-1)
                            self.figure.move_right()
                        elif event.key == K_LEFT:
                            walk_sound.play(-1)
                            self.figure.move_left()
                        elif event.key == K_UP:
                            self.figure.jump()
                        elif event.key == K_x:
                            self.current_level.set_action_key()
                            # lvl check
                            if self.figure.is_on_door:
                                self.figure.is_on_door = False
                                self.next_level()
                        elif event.key == K_ESCAPE:
                            if self.isRunning:
                                self.stop()
                                break
                            else:
                                # self.start()
                                self.stop()
                                break
                        elif event.key == K_p:
                            pygame.mixer.music.pause()
                            self.pause()
                        elif event.key == K_s:
                            if self.current_level_no == 0:
                                self.figure.inventory.append("chest-2")
                                self.figure.inventory.append("chest-1")
                                self.next_level()
                            
                    if event.type == KEYUP:
                        if event.key == K_LEFT and self.figure.change_x < 0:
                            self.figure.stop_moving()
                        if event.key == K_RIGHT and self.figure.change_x > 0:
                            self.figure.stop_moving()
                        walk_sound.stop()
                        self.current_level.set_action_key(False)

                self.current_level.update()
                self.current_level.draw(self.screen)

                # RENDERING & COLLISION DETECTION

                self.active_sprite_list.update()
                self.active_sprite_list.draw(self.screen)

                # If the player gets near the right side, shift the world left (-x)

                if self.figure.rect.right > self.v_res:
                    self.figure.rect.right = self.v_res

                # If the player gets near the left side, shift the world right (+x)
                if self.figure.rect.left < 0:
                    self.figure.rect.left = 0

                # GUI
                self.gui.draw_time(self.time)

                # time and heck life
                self.time -= self.TIME_TICK
                if self.time <= 0:
                    fall_sound.play(0)
                    self.figure.lives -= 1
                    self.time += 20
                if self.figure.lives <= 0:
                    game_over_sound.play(0)
                    walk_sound.stop()
                    pygame.mixer.music.stop()
                    self.gui.show_end()
                    self.isRunning = False

                # show lives
                self.gui.show_lives(self.figure.lives)

                # show inventory
                self.gui.show_inventory(self.figure)

                if self.figure.show_action_key:
                    self.gui.show_action_key()

                # tutorial
                self.tut_messages()

                pygame.display.flip()

            else:
                for event in pygame.event.get():
                    if not hasattr(event, 'key'):
                        continue
                    if event.type == KEYDOWN:
                        if event.key == K_ESCAPE:
                            # self.start()
                            self.stop()
                            break
                    elif event.key == K_o:
                        pygame.mixer.music.unpause()
                        self.continue_()

    def pause(self):
        self.isRunning = False

    def continue_(self):
        self.isRunning = True
    
    def stop(self):
        pygame.mixer.music.stop()
        self.isRunning = False
        self.run = False


    @staticmethod
    def quit():
        exit()
