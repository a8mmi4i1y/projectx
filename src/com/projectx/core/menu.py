import pygame
from pygame import *
font.init()

from math import cos, radians
from com.projectx.core.game import Game
from com.projectx.util.configuration import Configuration


def menu(menu, pos='center', font1=None, font2=None, color1=(128, 128, 128), color2=None, interline=5, justify=True,
         light=5, speed=300, lag=30):

    class Item(Rect):

        def __init__(self, menu, label):
            Rect.__init__(self, menu)
            self.label = label

    def show():
        i = Rect((0, 0), font2.size(menu[idx].label))
        if justify:
            i.center = menu[idx].center
        else:
            i.midleft = menu[idx].midleft
        display.update(
            (scr.blit(bg, menu[idx], menu[idx]), scr.blit(font2.render(menu[idx].label, 1, (255, 255, 255)), i)))

        time.wait(50)
        scr.blit(bg, r2, r2)
        [scr.blit(font1.render(item.label, 1, color1), item)
         for item in menu if item != menu[idx]]
        r = scr.blit(font2.render(menu[idx].label, 1, color2), i)
        display.update(r2)

        return r

    def anim():
        clk = time.Clock()
        a = [menu[0]] if lag else menu[:]
        c = 0
        while a:
            for i in a:
                g = i.copy()
                i.x = i.animx.pop(0)
                r = scr.blit(font1.render(i.label, 1, color1), i)
                display.update((g, r))

                scr.blit(bg, r, r)
            c += 1
            if not a[0].animx:
                a.pop(0)
                if not lag:
                    break
            if lag:
                foo, bar = divmod(c, lag)
                if not bar and foo < len(menu):
                    a.append(menu[foo])
            clk.tick(speed)

    events = event.get()
    scr = display.get_surface()
    scrrect = scr.get_rect()
    bg = scr.copy()
    if not font1:
        font1 = font.Font(None, scrrect.h // len(menu) // 3)
    if not font2:
        font2 = font1
    if not color1:
        color1 = (128, 128, 128)
    if not color2:
        color2 = list(map(lambda x: x + (255 - x) * light // 10, color1))
    m = max(menu, key=font1.size)
    r1 = Rect((0, 0), font1.size(m))
    ih = r1.size[1]
    r2 = Rect((0, 0), font2.size(m))
    r2.union_ip(r1)
    w, h = r2.w - r1.w, r2.h - r1.h
    r1.h = (r1.h + interline) * len(menu) - interline
    r2 = r1.inflate(w, h)

    try:
        setattr(r2, pos, getattr(scrrect, pos))
    except:
        r2.topleft = pos
    if justify:
        r1.center = r2.center
    else:
        r1.midleft = r2.midleft

    menu = [Item(((r1.x, r1.y + e * (ih + interline)), font1.size(i)), i)
            for e, i in enumerate(menu)if i]
    if justify:
        for i in menu:
            i.centerx = r1.centerx

    if speed:
        for i in menu:
            z = r1.w - i.x + r1.x
            i.animx = [
                cos(radians(x)) * (i.x + z) - z for x in list(range(90, -1, -1))]
            i.x = i.animx.pop(0)
        anim()
        for i in menu:
            z = scrrect.w + i.x - r1.x
            i.animx = [
                cos(radians(x)) * (-z + i.x) + z for x in list(range(0, -91, -1))]
            i.x = i.animx.pop(0)

    mpos = Rect(mouse.get_pos(), (0, 0))
    event.post(
        event.Event(MOUSEMOTION, {'pos': mpos.topleft if mpos.collidelistall(menu) else menu[0].center}))
    idx = -1
    display.set_caption("New Age")

    while True:

        ev = event.wait()
        if ev.type == MOUSEMOTION:
            idx_ = Rect(ev.pos, (0, 0)).collidelist(menu)
            if idx_ > -1 and idx_ != idx:
                idx = idx_
                r = show()
        elif ev.type == MOUSEBUTTONUP and r.collidepoint(ev.pos):
            ret = menu[idx].label, idx
            break
        elif ev.type == KEYDOWN:
            try:
                idx = (idx + {K_UP: -1, K_DOWN: 1}[ev.key]) % len(menu)
                r = show()
            except:
                if ev.key in (K_RETURN, K_KP_ENTER):
                    ret = menu[idx].label, idx
                    break
                # elif ev.key == K_ESCAPE:
                #    ret = None, None
                #    break
    scr.blit(bg, r2, r2)

    if speed:
        [scr.blit(font1.render(i.label, 1, color1), i) for i in menu]
        display.update(r2)
        time.wait(50)
        scr.blit(bg, r2, r2)
        anim()
    else:
        display.update(r2)

    for ev in events:
        event.post(ev)
    return ret


class GameMenu(object):
    def __init__(self):
        pygame.init()
        self.config = Configuration()
        self.background = image.load(self.config.get_string("menu_bg_image_path", "Graphics"))
        self.font_type = self.config.get_string("menu_font_path", "Font")
        self.bg_music = pygame.mixer.music.load(self.config.get_string("bg_menu_sound_path", "Sound"))
        self.v_res = self.config.get_integer("v_res", "Graphics")
        self.h_res = self.config.get_integer("h_res", "Graphics")
        self.game = Game()
        
    def run(self):
        from os.path import dirname, join
        
        time.Clock()

        self.bg_music = pygame.mixer.music.load(self.config.get_string("bg_menu_sound_path", "Sound"))
        
        if self.config.get_bool("bg_sound_enabled", "Sound"):
            pygame.mixer.music.play(-1)
            
        here = dirname(__file__)
        scr = display.set_mode((self.v_res, self.h_res))
        # print(menu.__doc__)
        f = font.Font(join(self.font_type), 50)
        f1 = font.Font(join(self.font_type), 35)
        f2 = font.Font(join(self.font_type), 15)
        font_options = font.Font(join(self.font_type), 25)
        mainmenu = f.render('New Age', 1, (130, 170, 230))
        r = mainmenu.get_rect()
        r.centerx, r.top = self.v_res/4, 40
        
        scr.blit(self.background, (0, 0))
        bg = scr.copy()
        scr.blit(mainmenu, r)
        display.flip()
 
        menu1 = {"menu": ['PLAY', 'OPTIONS', 'ABOUT', 'EXIT'], "font1": f1, "pos": (195, 150), "color1": (40, 40, 150), "light": 6, "speed": 200, "lag": 20}
        menu2 = {"menu": ['CONTINUE','NEW GAME', 'BACK'], "font1": f1, "pos": (185, 150), "color1": (40, 40, 150), "light": 5, "speed": 200, "lag": 20}
        menu3 = {"menu": ['BACK'], "pos": (30, self.h_res - 120), "color1": (40, 40, 150), "light": 5, "speed": 0, "font2": f1, "justify": 0}
        menu_options = {
        "menu": ['Enable sound', 'Disable sound', 'Low Graphics Details', 'High Graphics Details', 'BACK'],
        "font1": font_options, "pos": (150, 150),
                        "color1": (40, 40, 150), "light": 6, "speed": 200, "lag": 20}

        menus = (menu1, menu2, menu3)
        playlist = [menu1, menu2, menu3]

        resp = "re-show"
        while resp == "re-show":
            resp = menu(**menu1)[0]
    
        menu_running = True
        while menu_running:
            if resp == 'ABOUT':
                display.update(scr.blit(bg, r, r))
                title_about = f.render('***ABOUT***', 1, (130, 170, 230))
                display.update(scr.blit(title_about, (self.v_res/4 - title_about.get_rect().right/2, 40)))
                display.update(scr.blit(f1.render('New Age', 1, (255, 255, 255)), (40, 140)))
                display.update(scr.blit(f2.render('Version:   1.0', 1, (255, 255, 255)), (40, 190)))
                display.update(scr.blit(f2.render('Authors:', 1, (255, 255, 255)), (40, 210)))
                display.update(scr.blit(f2.render('                Bc. Roman Janas', 1, (255, 255, 255)), (40, 230)))
                display.update(scr.blit(f2.render('                Bc. Patrik Markovic', 1, (255, 255, 255)), (40, 250)))
                display.update(scr.blit(f2.render('                Bc. Tomas Kadavy', 1, (255, 255, 255)), (40, 270)))
                display.update(scr.blit(f2.render('Year:       2015', 1, (255, 255, 255)), (40, 290)))
                
                resp = menu(**menu3)[0]
    
            if resp == 'PLAY':
                display.update(scr.blit(bg, r, r))
                title_play = f.render('PLAY', 1, (130, 170, 230))
                display.update(scr.blit(title_play, (self.v_res/4 - title_play.get_rect().right/2, 40)))
                
                resp = menu(**menu2)[0]
               
            if resp == 'BACK':
                display.update(scr.blit(self.background, (0, 0)))
                title_main = f.render('New Age', 1, (130, 170, 230))
                display.update(scr.blit(title_main, (self.v_res/4 - title_main.get_rect().right/2, 40)))
                
                resp = menu(**menu1)[0]
            
            if resp == 'OPTIONS':
                resp = menu(**menu_options)[0]

            if resp == 'Enable sound':
                self.config.set_attr("Sound", "bg_sound_enabled", "True")
                resp = menu(**menu_options)[0]

            if resp == 'Disable sound':
                self.config.set_attr("Sound", "bg_sound_enabled", "False")
                resp = menu(**menu_options)[0]

            if resp == 'Low Graphics Details':
                self.config.set_attr("Graphics", "details_level", "0")
                resp = menu(**menu_options)[0]

            if resp == 'High Graphics Details':
                self.config.set_attr("Graphics", "details_level", "1")
                resp = menu(**menu_options)[0]
            
            if resp == 'NEW GAME':
                self.game = Game()
                self.game.start()
                pygame.mixer.music.stop()
                resp = None
            
            if resp == 'CONTINUE':
                self.game = Game(True)
                self.game.start()
                pygame.mixer.music.stop()
                resp = None
                
            if resp == 'EXIT':
                menu_running = False
                resp = None
                pygame.mixer.music.stop()
                self.game.quit()
                
            if self.game.run == False:
                self.run()
                pygame.mixer.music.play(-1)
                self.run()

