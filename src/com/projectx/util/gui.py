import pygame

from com.projectx.util.configuration import Configuration
from com.projectx.sprite.figure import FigureSprite


class Gui():

    _surface = None
    v_res = None
    h_res = None
    v_center = None
    h_center = None

    label = None
    font = None
    text_color = (125, 125, 125)
    text_background = (0, 0, 0)
    item_frame_color = (255, 255, 255)
    AA = True

    config = Configuration()

    image = pygame.image.load(config.get_string("live_img", "Gui"))
    inv_image = pygame.image.load(config.get_string("inventory_img", "Gui"))

    def __init__(self, surface, v_res, h_res):
        
        self._surface = surface
        self.h_res = h_res
        self.v_res = v_res
        
        self.v_center = self.v_res / 2
        self.h_center = self.h_res / 2
        
        self.font = pygame.font.SysFont("monospace", 30, True)
        self.label = self.font.render(" ", self.AA, self.text_color)
      
    def draw_time(self, time):

        txt_time = str(int(time))
        self.label = self.font.render(len(txt_time) * " ", self.AA, self.text_color, self.text_background)
        self._surface.blit(self.label, (self.v_center - self.label.get_rect().right,
                                        self.h_center - self.label.get_rect().bottom - 250))
        self.label = self.font.render(txt_time, self.AA, self.text_color)
        self._surface.blit(self.label, (self.v_center - self.label.get_rect().right,
                                        self.h_center - self.label.get_rect().bottom - 250))

    def show_inventory(self, player: FigureSprite):
        # first background and then items
        i = 0
        while i < player.INVENTORY_CAPACITY:
            self._surface.blit(self.inv_image, (25 + 50 * i, self.h_res - 75))
            i += 1
        i = 0
        while i < len(player.inventory):
            # placeholder for image of items
            item_type, item_id = player.inventory[i].split('-')
            inv_item = pygame.image.load(self.config.get_string("inventory_" + item_type, "Gui") + item_id + ".png")
            self._surface.blit(inv_item, (25 + 50 * i, self.h_res - 75))
            i += 1

    def show_lives(self, lives):
        # placeholder for live graphic (50x50 image)
        l = 0
        while l < lives:
            self._surface.blit(self.image, (self.v_res - (50 * (4 - l)), self.h_res - 75))
            l += 1
        pass

    def show_end(self):
        fon = pygame.font.SysFont("monospace", 70, True)
        fon2 = pygame.font.SysFont("monospace", 40, True)
        lab = fon.render("Game over", self.AA, (200, 50, 50))
        lab2 = fon2.render("Press ESC to return menu", self.AA, (200, 50, 50))
        self._surface.blit(lab, (self.v_center - lab.get_rect().right/2,
                                 self.h_center - lab.get_rect().bottom/2))
        self._surface.blit(lab2, (self.v_center - lab2.get_rect().right/2,
                                self.h_center + self.h_center/2 - lab2.get_rect().bottom/2))

    def show_skip(self):
        fon = pygame.font.SysFont("monospace", 20, True)
        lab1 = fon.render("You can skip this tutorial by pressing S", self.AA, (255, 255, 255))
        self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, self.h_res - lab1.get_rect().bottom))
        pass

    def show_action_key(self):
        fon = pygame.font.SysFont("monospace", 40, True)
        lab = fon.render("Press X key", self.AA, (200, 50, 50))
        self._surface.blit(lab, (self.v_center - lab.get_rect().right / 2,
                                 self.h_res - 75))
    def show_skip_lvl(self):
        fon = pygame.font.SysFont("monospace", 30, True)
        lab = fon.render("For skip tutorial press S...", self.AA, (200, 50, 50))
        self._surface.blit(lab, (self.v_center - lab.get_rect().right / 2,
                                 self.h_res - 30))

    def show_tut_message(self, n):
        fon = pygame.font.SysFont("monospace", 40, True)
        if n == 1:
            lab = fon.render("Welcome hero, your live will be short...", self.AA, (150, 50, 50))
            self._surface.blit(lab, (self.v_center - lab.get_rect().right / 2, 75))
        if n == 2:
            lab1 = fon.render("You can move by arrow keys,", self.AA, (150, 50, 50))
            lab2 = fon.render("can you do it?", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 3:
            lab1 = fon.render("Well done looser", self.AA, (150, 50, 50))
            lab2 = fon.render("In each level, you must find door", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 4:
            lab1 = fon.render("For pass to next level", self.AA, (150, 50, 50))
            lab2 = fon.render("you also need two crystals", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 5:
            lab1 = fon.render("You can find one in this chest", self.AA, (150, 50, 50))
            lab2 = fon.render("", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 6:
            lab1 = fon.render("To open chest, you need", self.AA, (150, 50, 50))
            lab2 = fon.render("right key", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 7:
            lab1 = fon.render("Like this one", self.AA, (150, 50, 50))
            lab2 = fon.render("", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 8:
            lab1 = fon.render("Open the chest and take", self.AA, (150, 50, 50))
            lab2 = fon.render("the crystal", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 9:
            lab1 = fon.render("You are quite funny", self.AA, (150, 50, 50))
            lab2 = fon.render("I give you second crystal", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))
        if n == 10:
            lab1 = fon.render("No go and", self.AA, (150, 50, 50))
            lab2 = fon.render("DIE IN PAIN", self.AA, (150, 50, 50))
            self._surface.blit(lab1, (self.v_center - lab1.get_rect().right / 2, 75))
            self._surface.blit(lab2, (self.v_center - lab2.get_rect().right / 2, 75 + lab1.get_rect().bottom + 10))

        pass