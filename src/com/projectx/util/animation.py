from os import listdir
from os.path import isfile, join

import pygame

from com.projectx.util.configuration import Configuration
from com.projectx.util.enum import Enum


class Animation():
    def __init__(self, folder=""):
        """
        required: figure in image is meant to be directed right
        """
        self.config = Configuration()
        self.action = Enum.action().walk
        self.direction = Enum.direction().right
        self.counter = 0
        self.count_to = self.config.get_integer("img_change_speed", "Animation")
        self.allowed_image_types = self.config.get_string("allowed_image_types", "Animation")
        self.folder = folder
        self.img_move = []
        self.img_jump = []
        self.img_crouch = []
        self.index = 0
        self._load_images()
        self.details_level = self.config.get_integer("details_level", "Graphics")

    def get_image(self):
        """
        return actual image based on actual action
        """
        if self.details_level is 1:
            if self.action == Enum.action().jump:
                image = pygame.image.load(self.img_jump[self.index])
                if self.direction == Enum.direction().left:
                    image = pygame.transform.flip(image, True, False)
            elif self.action == Enum.action().crouch:
                image = pygame.image.load(self.img_crouch[self.index])
                if self.direction == Enum.direction().left:
                    image = pygame.transform.flip(image, True, False)
            elif self.action == Enum.action().run or self.action == Enum.action().walk:
                image = pygame.image.load(self.img_move[self.index])
                if self.direction == Enum.direction().left:
                    image = pygame.transform.flip(image, True, False)
            else:
                image = pygame.image.load(self.img_move[0])
        else:
            if self.action == Enum.action().jump:
                image = pygame.image.load(self.img_jump[0])
                if self.direction == Enum.direction().left:
                    image = pygame.transform.flip(image, True, False)
            elif self.action == Enum.action().crouch:
                image = pygame.image.load(self.img_crouch[0])
                if self.direction == Enum.direction().left:
                    image = pygame.transform.flip(image, True, False)
            elif self.action == Enum.action().run or self.action == Enum.action().walk:
                image = pygame.image.load(self.img_move[0])
                if self.direction == Enum.direction().left:
                    image = pygame.transform.flip(image, True, False)
            else:
                image = pygame.image.load(self.img_move[0])
        return image

    def update_counter(self):
        if self.details_level is 1:
            if self.counter < self.count_to:
                self.counter += 1
            else:
                self.counter = 0
            if self.action == Enum.action().jump:
                if self.counter % (self.count_to / len(self.img_jump)) == 0:
                    self._update_index()
            elif self.action == Enum.action().crouch:
                if self.counter % (self.count_to / len(self.img_crouch)) == 0:
                    self._update_index()
            elif self.action == Enum.action().run or self.action == Enum.action().walk:
                if self.counter % (self.count_to / len(self.img_move)) == 0:
                    self._update_index()


    def set_direction(self, direction):
        if self.direction != direction:
            self.direction = direction
            self._update_index(True)

    def _set_action(self, action):
        if self.action != action:
            self.action = action
            self._update_index(True)

    def toggle_crouch(self):
        if self.action == Enum.action().crouch:
            self.action = Enum.action().walk
            self._update_index(True)
            return True
        else:
            self.action = Enum.action().crouch
            self._update_index(True)
            return False

    def toggle_jump(self):
        if self.action == Enum.action().jump:
            self.action = Enum.action().walk
        else:
            self.action = Enum.action().jump

    def _load_images(self):
        """
        load files from defined directory

        these files will automatically rotate when figure is moving,
        automatically set first image and
        """
        self.img_move = [self.folder + '/move/' + f for f in listdir(self.folder + '/move')
                         if isfile(join(self.folder + '/move', f)) and f[-3:] in self.allowed_image_types]
        self.img_jump = [self.folder + '/jump/' + f for f in listdir(self.folder + '/jump')
                         if isfile(join(self.folder + '/jump', f)) and f[-3:] in self.allowed_image_types]
        self.img_crouch = [self.folder + '/crouch/' + f for f in listdir(self.folder + '/crouch')
                           if isfile(join(self.folder + '/crouch', f)) and f[-3:] in self.allowed_image_types]
        if len(self.img_move) > 0:
            self.image = pygame.image.load(self.img_move[self.index])

    def _update_index(self, reset=False):
        if reset:
            self.index = 0
        if self.action == Enum.action().walk or self.action == Enum.action().run:
            if self.index < len(self.img_move) - 1:
                self.index += 1
            else:
                self.index = 0
        if self.action == Enum.action().jump:
            if self.index < len(self.img_jump) - 1:
                self.index += 1
            else:
                self.index = 0
        if self.action == Enum.action().crouch:
            if self.index < len(self.img_crouch) - 1:
                self.index += 1
            else:
                self.index = 0