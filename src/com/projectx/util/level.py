from random import randint
import pygame

from com.projectx.sprite import platform
from com.projectx.sprite import key
from com.projectx.sprite import chest
from com.projectx.sprite import door
from com.projectx.util.configuration import Configuration


class Level(object):
    """ This is a generic super-class used to define a level.
        Create a child class for each level with level-specific
        info. """

    def __init__(self, player):
        """ Constructor. Pass in a handle to player. Needed for when moving platforms
            collide with the player. """
        self.platform_list = pygame.sprite.Group()
        self.key_list = pygame.sprite.Group()
        self.chest_list = pygame.sprite.Group()
        self.player = player
        self.config = Configuration()
        self.action_key_pressed = False
        self.details_level = self.config.get_integer("details_level", "Graphics")
        # door for end level snad go to next
        self.door_list = pygame.sprite.Group()

    # Update everything on this level
    def update(self):
        """ Update everything in this level."""
        self.platform_list.update()
        self.key_list.update(self.player.key_collisions, self.action_key_pressed, self.player)
        self.chest_list.update(self.player.chest_collisions, self.action_key_pressed, self.player)
        self.door_list.update(self.player.door_collisions, self.action_key_pressed, self.player)

    def draw(self, screen):
        """ Draw everything on this level. """

        # Draw the background
        #screen.blit(self.background, (0, 0))
        if self.details_level is 1:
            screen.blit(self.background, (0, 0))
        else:
            screen.fill((100, 255, 100))

        # Draw all the sprite lists that we have
        self.platform_list.draw(screen)
        self.key_list.draw(screen)
        self.chest_list.draw(screen)
        self.door_list.draw(screen)

    def set_action_key(self, pressed=True):
        self.action_key_pressed = pressed

class Random_level(Level):
    def __init__(self, player):
        """ Create random level """

        # Call the parent constructor
        Level.__init__(self, player)

        # Defines important variables
        w = 1024    # width
        h = 388     # height - player_zero_floor - player(32) + space(20)
        h_space = 70# player + space + platform
        p_w = 128   # width of platform
        p_h = 18    # height of platform
        r = 6       # h/(p_h + player_height + space_above_player)
        c = 8       # w/p_w
        d_w = 20     # delta w
        d_h = 0     # delta h

        # Random background
        self.background = pygame.image.load(self.config.get_string("bg_"+str(randint(0, 3))+"_image_path", "Graphics"))

        # Array with x and y of platform
        # Random numbers of platforms
        platforms = []

        num_door = 1
        num_key = 2
        num_chest = 2
        ocupied = []

        # generate number of structures
        for i in range(0, c - 1):
            if randint(0, 1) == 1:
                for j in range(0, randint(1, r - 1)):
                    p = [(i * p_w) + (randint(-d_w, d_w)), (h - p_h) - (j * h_space) + randint(-d_h, d_h)]
                    platforms.append(p)
                    ocupied.append(i)
                    print(str(p)+", pos:"+str(i))

        # generate random platforms on free spaces
        last = 0
        for i in range(0, c - 1):
            for j in range(1, len(ocupied)):
                if i == ocupied[j]:
                    i += 1
                    break
            p = [(i * p_w) + (randint(-d_w, d_w)), (h - p_h) - (randint(1, 3) * h_space) + randint(-d_h, d_h)]
            platforms.append(p)

        keys = []
        # add keys
        for i in range(0, num_key):
            p = platforms[randint(0, len(platforms) - 1)]
            k = [p[0] + (p_w / 2), p[1] - p_h, i + 1]
            keys.append(k)

        chests = []
        # add chests
        for i in range(0, num_chest):
            p = platforms[randint(0, len(platforms) - 1)]
            ch = [p[0] + (p_w / 2), p[1] - p_h, i + 1]
            chests.append(ch)

        # door
        p = platforms[randint(0, len(platforms) - 1)]
        d = door.Door((p[0] + (p_w / 2), p[1] - p_h))
        self.door_list.add(d)

        print("platforms: "+str(len(platforms)))


        # Go through the array above and add platforms
        for plat in platforms:
            block = platform.Platform((plat[0], plat[1]))
            self.platform_list.add(block)


        for kk in keys:
            block = key.Key((kk[0], kk[1]), kk[2])
            self.key_list.add(block)


        for ch in chests:
            block = chest.ChestSprite((ch[0], ch[1]), ch[2])
            self.chest_list.add(block)



class Level_tut_01(Level):
    """ Definition for level 1. """

    def __init__(self, player):
        """ Create level 1. """

        # Call the parent constructor
        Level.__init__(self, player)

        self.background = pygame.image.load(self.config.get_string("bg_tut_image_path", "Graphics"))
        # Array with x and y of platform
        platforms = []

        # Go through the array above and add platforms
        for plat in platforms:
            block = platform.Platform((plat[0], plat[1]))
            self.platform_list.add(block)

        keys = []

        for kk in keys:
            block = key.Key((kk[0], kk[1]), kk[2])
            self.key_list.add(block)

        chests = []

        for ch in chests:
            block = chest.ChestSprite((ch[0], ch[1]), ch[2])
            self.chest_list.add(block)
        self.enabled_door = False
        self.enabled_chest = False
        self.enabled_key = False

    def enable_door(self):
        if self.enabled_door:
            return None
        d = door.Door((150, 415))
        self.door_list.add(d)
        self.enabled_door = True

    def enable_chest(self):
        if self.enabled_chest:
            return None
        c = chest.ChestSprite((600, 415), 1)
        self.chest_list.add(c)
        self.enabled_chest = True

    def enable_key(self):
        if self.enabled_key:
            return None
        k = key.Key((470, 350), 1)
        p = platform.Platform((450, 370))
        self.platform_list.add(p)
        self.key_list.add(k)
        self.enabled_key = True


class Level_01(Level):
    """ Definition for level 1. """

    def __init__(self, player):
        """ Create level 1. """

        # Call the parent constructor
        Level.__init__(self, player)

        self.background = pygame.image.load(self.config.get_string("bg_1_image_path", "Graphics"))
        # Array with x and y of platform
        platforms = [[200, 260], 
                     [400, 320],
                     [280, 380], 
                     [530, 260],
                     [630, 200], 
                     [510, 140],
                     [400, 80]]

        # Go through the array above and add platforms
        for plat in platforms:
            block = platform.Platform((plat[0], plat[1]))
            self.platform_list.add(block)

        keys = [[440, 60, 1],
                [220, 240, 2]]

        for kk in keys:
            block = key.Key((kk[0], kk[1]), kk[2])
            self.key_list.add(block)

        chests = [[290, 250, 1],
                  [620, 250, 2]]

        for ch in chests:
            block = chest.ChestSprite((ch[0], ch[1]), ch[2])
            self.chest_list.add(block)

        d = door.Door((150, 415))
        self.door_list.add(d)

class Level_02(Level):
    """ Definition for level 2. """

    def __init__(self, player):
        """ Create level 2. """

        # Call the parent constructor
        Level.__init__(self, player)

        self.background = pygame.image.load(self.config.get_string("bg_2_image_path", "Graphics"))

        # Array with x and y of platform
        platforms = [[10, 400],
                     [900, 380],
                     [-80, 310],
                     [160, 240],
                     [160, 160],
                     [400, 140],
                     [600, 230],
                     [800, 320],
                     [670, 130],
                     [900, 60]]

        # Go through the array above and add platforms
        for plat in platforms:
            block = platform.Platform((plat[0], plat[1]))
            self.platform_list.add(block)

        keys = [[440, 120, 1],
                [850, 300, 2]]

        for kk in keys:
            block = key.Key((kk[0], kk[1]), kk[2])
            self.key_list.add(block)

        chests = [[270, 230, 1],
            [940, 50, 2]]

        for ch in chests:
            block = chest.ChestSprite((ch[0], ch[1]), ch[2])
            self.chest_list.add(block)

        d = door.Door((200, 140))
        self.door_list.add(d)
        
class Level_03(Level):
    """ Definition for level 3. """

    def __init__(self, player):
        """ Create level 3. """

        # Call the parent constructor
        Level.__init__(self, player)

        self.background = pygame.image.load(self.config.get_string("bg_3_image_path", "Graphics"))

        # Array with x and y of platform
        platforms = [[160, 160],
                     [400, 230],
                     [200, 320],
                     [300, 70],
                     [900, 60],
                     [860, 140],
                     [900, 220],
                     [860, 300],
                     [900, 380],
                     [50, 380]]
                     

        # Go through the array above and add platforms
        for plat in platforms:
            block = platform.Platform((plat[0], plat[1]))
            self.platform_list.add(block)

        keys = [[960, 200, 1],
                [400, 50, 2]]

        for kk in keys:
            block = key.Key((kk[0], kk[1]), kk[2])
            self.key_list.add(block)

        chests = [[320, 60, 1],
                  [910, 290, 2]]

        for ch in chests:
            block = chest.ChestSprite((ch[0], ch[1]), ch[2])
            self.chest_list.add(block)

        d = door.Door((970, 40))
        self.door_list.add(d)
