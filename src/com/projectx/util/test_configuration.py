from unittest import TestCase

from com.projectx.util.configuration import Configuration


class TestConfiguration(TestCase):
    def setUp(self):
        self.configuration = Configuration("../config")

    def test_get_string(self):
        self.assertTrue(isinstance(self.configuration.get_string("version", "General"), str), "not a string")

    def test_get_string_value(self):
        self.assertTrue(self.configuration.get_string("version") == "0.1", "version is not 0.1")

    def test_set_attr(self):
        self.setUp()
        self.configuration.set_attr("Sound", "bg_sound_enabled", "False")