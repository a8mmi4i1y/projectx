import unittest
from unittest import TestCase

import pygame

from com.projectx.util.animation import Animation
from com.projectx.util.enum import Enum


class TestAnimation(TestCase):
    def setUp(self):
        self.animation = Animation("../img/figure")

    def test_init(self):
        self.assertTrue(isinstance(self.animation, Animation), 'not instance of Animation')

    def test_get_image(self):
        self.animation._load_images()
        self.assertTrue(isinstance(self.animation.get_image(), pygame.Surface),
            'wrong image instance (not pygame.Surface)')

    def test_move_enum(self):
        self.assertTrue(Enum.direction().right == 1, "wrong move enum (or changed)")
        self.assertTrue(Enum.action().jump == 2, "wrong action enum (or changed)")


if __name__ == '__main__':
    unittest.main()