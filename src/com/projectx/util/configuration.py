from os import listdir
from os.path import isfile, join

from configparser import ConfigParser


class Configuration(object):
    def __init__(self, folder="config"):
        """
        Loads configs from every file in defined folder

        When one attribute and section combination appears in more than one config, the last occurrence
        will be used (from alphabetically sorted) last file in folder.
        There should be one production file (the default one) and other for developing / testing purposes.
        Once your code is tested, move your config to production and to _default_configuration()
        :param folder: where are config files stored
        """
        self.default_filename = "production.cfg"
        self.config = None
        self.folder = folder
        self.files_to_parse = None

    def _default_configuration(self):
        """
        There are default config records.
        After your code is tested, add your own settings - default file is recreated every time there is no config
        file found.
        """
        section = "General"
        self.config.add_section(section)
        self.config.set(section, "version", "1.0")
        self.config.set(section, "game_level", "0")
        section = "Graphics"
        self.config.add_section(section)
        self.config.set(section, "v_res", "1024")
        self.config.set(section, "h_res", "575")
        self.config.set(section, "details_level", "1")  # 0 RPi / 1 PC
        self.config.set(section, "bg_0_image_path", "img/l0_1024.png")
        self.config.set(section, "bg_1_image_path", "img/l1_1024.jpg")
        self.config.set(section, "bg_2_image_path", "img/l2_1024.jpg")
        self.config.set(section, "bg_3_image_path", "img/l3_1024.png")
        self.config.set(section, "bg_tut_image_path", "img/l_tut.jpg")
        self.config.set(section, "menu_bg_image_path", "img/menu_background_1024.jpg")
        self.config.set(section, "platform_image_path", "img/platform_128.png")
        self.config.set(section, "key_image_path", "img/key")
        self.config.set(section, "door_image_path", "img/door.png")
        section = "Sound"
        self.config.add_section(section)
        self.config.set(section, "bg_sound_path", "sound/bg-music-2.mp3")
        self.config.set(section, "bg_menu_sound_path", "sound/menu_bg_music.mp3")
        self.config.set(section, "bg_sound_enabled", "True")
        self.config.set(section, "walk_sound_path", "sound/walk.ogg")
        self.config.set(section, "jump_sound_path", "sound/jump.ogg")
        self.config.set(section, "fall_sound_path", "sound/fall_hit.ogg")
        self.config.set(section, "chest_sound_path", "sound/opening_chest.ogg")
        self.config.set(section, "game_over_sound_path", "sound/game_over.ogg")
        self.config.set(section, "key_sound_path", "sound/key_drop.ogg")
        section = "Animation"
        self.config.add_section(section)
        self.config.set(section, "allowed_image_types", "[\"jpg\", \"png\", \"gif\"]")
        self.config.set(section, "img_change_speed", "5")
        section = "Figure"
        self.config.add_section(section)
        self.config.set(section, "movement_speed", "3")
        self.config.set(section, "gravity", "0.35")
        self.config.set(section, "crouch_speed_modifier", "3")
        section = "Gui"
        self.config.add_section(section)
        self.config.set(section, "live_img", "img/live.png")
        self.config.set(section, "inventory_img", "img/inventory/inventory_back.png")
        self.config.set(section, "inventory_key", "img/inventory/key/")
        self.config.set(section, "inventory_chest", "img/inventory/chest/")
        section = "Font"
        self.config.add_section(section)
        self.config.set(section, "menu_font_path", "font/FEASFBRG.ttf")

    def set_attr(self, section, attr, value):
        if self.config is None:
            self.config = ConfigParser()
            self._load_configurations()
        self.config.set(section, attr, value)
        self._save_configuration()

    def get_string(self, attr, section="General"):
        """
        Return attribute (from section) as a string representation

        :param section: not required - should be used if there is more attributes with equivalent name
        (or in special section)
        :param attr: name of attribute to return
        :return: value for input attribute
        """
        if self.config is None:
            self.config = ConfigParser()
            self._load_configurations()
        return self.config.get(section, attr)

    def get_integer(self, attr, section="General"):
        """
        Return attribute (from section) as a integer

        :param section: not required - should be used if there is more attributes with equivalent name
        (or in special section)
        :param attr: name of attribute to return
        :return: value for input attribute
        """
        if self.config is None:
            self.config = ConfigParser()
            self._load_configurations()
        return self.config.getint(section, attr)
    
    def get_float(self, attr, section="General"):
        """
        Return attribute (from section) as a float

        :param section: not required - should be used if there is more attributes with equivalent name
        (or in special section)
        :param attr: name of attribute to return
        :return: value for input attribute
        """
        if self.config is None:
            self.config = ConfigParser()
            self._load_configurations()
        return self.config.getfloat(section, attr)

    def get_bool(self, attr, section="General"):
        """
        Return attribute (from section) as a boolean

        :param section: not required - should be used if there is more attributes with equivalent name
        (or in special section)
        :param attr: name of attribute to return
        :return: value for input attribute
        """
        if self.config is None:
            self.config = ConfigParser()
            self._load_configurations()
        return self.config.getboolean(section, attr)

    def _load_configurations(self):
        self._set_files_to_parse()
        if len(self.files_to_parse) > 0:
            for f in self.files_to_parse:
                self.config.read(f)
        else:
            self._default_configuration()
            self._save_configuration()

    def _set_files_to_parse(self):
        self.files_to_parse = [self.folder + '/' + f for f in listdir(self.folder + "/")
                               if isfile(join(self.folder + "/", f)) and f[-3:] in ["cfg"]]

    def _save_configuration(self):
        if self.config is not None:
            with open(self.folder + '/' + self.default_filename, 'w+') as configfile:
                self.config.write(configfile)