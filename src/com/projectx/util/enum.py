class Enum():
    def enum(*sequential, **named):
        enums = dict(zip(sequential, range(len(sequential))), **named)
        return type('Enum', (), enums)

    @staticmethod
    def direction():
        """
        Contains all possible ways to move
        """
        return Enum.enum('left', 'right')

    @staticmethod
    def action():
        """
        Contains all possible actions
        TODO run?
        """
        return Enum.enum('walk', 'run', 'jump', 'crouch', 'none')